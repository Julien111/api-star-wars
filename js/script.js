//Afficher les personnages

let bloc = document.getElementById("affiche");

for (let i = 1; i < 16; i++) {
  let result = "https://swapi.dev/api/people/" + i + "/";

  //console.log(result);

  const carteElt = document.createElement("div");

  carteElt.classList.add("personnage");

  fetch(result)
    .then((response) => response.json())
    .then((data) => {
      //console.log(data);

      const blocCarte = `<div class='perso'><h2><strong>Nom :</strong> ${data.name}</h2><p><strong>Hauteur :</strong> ${data.height} cm</p><p><strong>Genre :</strong> ${data.gender}</p><p><strong>Poids : </strong>${data.mass} kg</p></div>`;

      carteElt.innerHTML = blocCarte;

      bloc.appendChild(carteElt);
    });
}

//Afficher les planètes

let bloc2 = document.getElementById("affichePlanete");

for (let i = 1; i < 16; i++) {
  let url = "https://swapi.dev/api/planets/" + i + "/";

  const carteSpecies = document.createElement("div");

  carteSpecies.classList.add("species");

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      //console.log(data);

      const blocPlanete = `<div class='planete'><h2><strong>Nom :</strong> ${data.name}</h2><p><strong>Nombre d'heures dans une journée : </strong>${data.rotation_period} H</p><p><strong>Diamètre :</strong> ${data.diameter} km</p><p><strong>Climat :</strong> ${data.climate}</p><p><strong>Type de terrain :</strong> ${data.terrain}.</p></div>`;

      carteSpecies.innerHTML = blocPlanete;

      bloc2.appendChild(carteSpecies);
    });
}

//Afficher les vaisseaux star wars

let bloc3 = document.getElementById("afficheVaisseau");

for (let i = 1; i < 10; i++) {
  let url2 = "https://swapi.dev/api/starships/";

  const carteVaisseau = document.createElement("div");

  carteVaisseau.classList.add("vaisseau");

  fetch(url2)
    .then((response) => response.json())
    .then((data) => {
      //console.log(data);

      const blocPlanete = `<div class='starship'><h2><strong>Nom du vaisseau:</strong> ${data.results[i].name}</h2><p><strong>Modèle :</strong> ${data.results[i].model} H</p><p><strong>Manufacturier :</strong> ${data.results[i].manufacturer}</p><p><strong>Nombre de passagers :</strong> ${data.results[i].passengers}</p></div>`;

      carteVaisseau.innerHTML = blocPlanete;

      bloc3.appendChild(carteVaisseau);
    });
}

//Afficher les informations sur les espèces (star wars)

let bloc4 = document.getElementById("afficheEspece");

for (let i = 1; i < 10; i++) {
  let url4 = "https://swapi.dev/api/species/?page=1";

  const carteEspece = document.createElement("div");

  carteEspece.classList.add("espece");

  fetch(url4)
    .then((response) => response.json())
    .then((donnees) => {
      // console.log(donnees);

      const blocEspece = `<div class='espece'><h2><strong>Nom de l'espèce:</strong> ${donnees.results[i].name}</h2><p><strong>Classification :</strong> ${donnees.results[i].classification} H</p><p><strong>Langue :</strong> ${donnees.results[i].language}</p><p><strong>Taille moyenne :</strong> ${donnees.results[i].average_height} cm</p></div>`;

      carteEspece.innerHTML = blocEspece;

      bloc4.appendChild(carteEspece);
    });
}

//test afficher plus d'espèces

for (let i = 1; i < 10; i++) {
  let url6 = "https://swapi.dev/api/species/?page=3";

  const carteEspece2 = document.createElement("div");

  carteEspece2.classList.add("espece");

  fetch(url6)
    .then((response) => response.json())
    .then((donnees) => {
      //console.log(donnees);

      const blocEspece1 = `<div class='espece'><h2><strong>Nom de l'espèce:</strong> ${donnees.results[i].name}</h2><p><strong>Classification :</strong> ${donnees.results[i].classification} H</p><p><strong>Langue :</strong> ${donnees.results[i].language}</p><p><strong>Taille moyenne :</strong> ${donnees.results[i].average_height} cm</p></div>`;

      carteEspece2.innerHTML = blocEspece1;

      bloc4.appendChild(carteEspece2);
    });
}

//Les véhicules ont le même type d'informations

let bloc5 = document.getElementById("afficheCar");

for (let i = 1; i < 9; i++) {

  let url5 = "https://swapi.dev/api/vehicles/?page=4";

  const carteVehi = document.createElement("div");

  carteVehi.classList.add("car");

  fetch(url5)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);

      const blocCar = `<div class='vehi'><h2><strong>Nom du véhicule:</strong> ${data.results[i].name}</h2><p><strong>Modèle :</strong> ${data.results[i].model} </p><p><strong>Manufacturier :</strong> ${data.results[i].manufacturer}</p><p><strong>Nombre de passagers :</strong> ${data.results[i].passengers}</p></div>`;

      carteVehi.innerHTML = blocCar;

      bloc5.appendChild(carteVehi);
    });
}
